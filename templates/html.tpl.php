<?php

/**
 * @file
 * Default theme implementation to display the basic html structure of a single
 * Drupal page.
 *
 * Variables:
 * - $css: An array of CSS files for the current page.
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.
 * - $rdf_namespaces: All the RDF namespace prefixes used in the HTML document.
 * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF data.
 * - $head_title: A modified version of the page title, for use in the TITLE tag.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $page_top: Initial markup from any modules that have altered the
 *   page. This variable should always be output first, before all other dynamic
 *   content.
 * - $page: The rendered page content.
 * - $page_bottom: Final closing markup from any modules that have altered the
 *   page. This variable should always be output last, after all other dynamic
 *   content.
 * - $classes String of classes that can be used to style contextually through
 *   CSS.
 *
 * @see template_preprocess()
 * @see template_preprocess_html()
 * @see template_process()
 */
?><!DOCTYPE html>
<!--[if IEMobile 7 ]><html class="no-js iem7" manifest="default.appcache?v=1" <?php if(isset($html_attributes)) {print $html_attributes . $rdf_namespaces;}; ?>><![endif]-->
<!--[if lt IE 7 ]><html class="no-js ie6" lang="en" <?php if(isset($html_attributes)) {print $html_attributes . $rdf_namespaces;}; ?>><![endif]-->
<!--[if IE 7 ]><html class="no-js ie7" lang="en" <?php if(isset($html_attributes)) {print $html_attributes . $rdf_namespaces;}; ?>><![endif]-->
<!--[if IE 8 ]><html class="no-js ie8" lang="en" <?php if(isset($html_attributes)) {print $html_attributes . $rdf_namespaces;}; ?>><![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!-->
<html class="no-js" <?php if(isset($html_attributes)) {print $html_attributes . $rdf_namespaces;}; ?>>
<!--<![endif]-->
<head>
  <meta charset="utf-8">
  <title><?php print html_entity_decode($head_title); ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- http://t.co/dKP3o1e -->
  <meta name="HandheldFriendly" content="True">
  <meta name="MobileOptimized" content="320">
  <meta name="viewport" content="width=device-width, target-densitydpi=160dpi, initial-scale=1">

  <!-- For all browsers -->
  <?php if(isset($media_queried_css)){print $media_queried_css;}; ?>

  <?php if (!$nojs): ?>
    <!-- HTML5ifer + HTML5 feature detection (from magma theme) -->
    <script src="/<?php print drupal_get_path('theme', 'magma'); ?>/scripts/modernizr.custom.min.js"></script>
  <?php endif; ?>

  <!-- For IE < 9 -->
  <?php print $styles; ?>

  <!-- For iPhone 4 -->
  <link rel="apple-touch-icon" sizes="114x114" href="/<?php print path_to_theme(); ?>/images/icons/icon114.png">
  <!-- For iPad 1-->
  <link rel="apple-touch-icon" sizes="72x72" href="/<?php print path_to_theme(); ?>/images/icons/icon72.png">
  <!-- For iPhone 3G, iPod Touch and Android -->
  <link rel="apple-touch-icon" href="/<?php print path_to_theme(); ?>/images/icons/icon57.png">
  <!-- For Nokia -->
  <link rel="shortcut icon" href="/<?php print path_to_theme(); ?>/images/icons/icon57.png">
  <!-- For everything else -->
  <link rel="shortcut icon" href="/<?php print path_to_theme(); ?>/images/icons/favicon.ico">

  <?php if ($is_front): ?>
  <link rel="canonical" href="/">
  <?php endif; ?>

  <?php print $head; ?>
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>

  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>

  <?php if (!$nojs): ?>
    <?php print $scripts; ?>
  <?php endif; ?>
</body>
</html>