(function($) {
  'use strict';

  /**
   *  @description: Adding Mobile Menu
   */
  function addMobileMenu() {

    var mobile_menu   = $('.mobile-menu'); 
    var isVisible     = mobile_menu.is(':visible');
    var target_menu   = $('#block-deewr-book-customisation-tagged-toc'); 
    var isMenuVisible = target_menu.is(':visible');
    var screenSize    = $(window).width; 

    if(isVisible) {
      target_menu.addClass("is-hidden");
      mobile_menu.click(function(){
        event.preventDefault();
        target_menu.toggleClass('is-hidden', 1000);
        target_menu.toggleClass('blue-bg');
      });
    }

    $(window).resize(function(){
      if(screenSize < 768) {
        target_menu.addClass("is-hidden");
        target_menu.addClass("blue-bg");
        mobile_menu.click(function(){
          event.preventDefault();
          target_menu.toggleClass('is-hidden', 1000);
          target_menu.toggleClass('blue-bg');
        });
      }
      else {
        target_menu.removeClass("blue-bg");
      }
    });
  }
  $(addMobileMenu);

  /**
   * @description: detect active 
   */

  // function detectActiveMenuItem() {
  //   var path = window.location.pathname;
  //   path = path.replace(/\/$/, "");
  //   path = decodeURIComponent(path);

  //   $(".book-toc-menu a").each(function() {
  //       var href = $(this).attr('href');
  //       if (path.substring(0, href.length) === href) {
  //           $(this).closest('li a').addClass('active');
  //       }
  //   });
  // }
  // $(detectActiveMenuItem); //Single


  function getPath() {
    var path = window.location.pathname;
    path = path.replace(/\/$/, "");
    path = decodeURIComponent(path);
    return path;
  }

  function getActiveMenuItemBySelectors(selector) {
    var path = getPath();
    var $menuItems = $(selector);
    var $activeMenuItem = $menuItems.filter(function(i, el) {
      var $menuItem = $(el);
      var $a = $menuItem.find("a"); 
      $a.each(function(i) {
        var link = $a[i];
        var linkPath = link.pathname;
        if (path.substring(0, linkPath.length) === linkPath) {
          $(this).closest('li').addClass('active');
        }
      });
    });
  }

  $(getActiveMenuItemBySelectors(['.book-toc-menu li']));


}(jQuery));
