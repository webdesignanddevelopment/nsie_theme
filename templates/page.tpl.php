<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/garland.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 */
?>
<div class="page-info">
 <h2 class="visuallyhidden">Site Information and assistance</h2>
 <?php /* We can't use anchors in Drupal menus without a plugin, so this menu is manual */ ?>
 <ul>
   <li><a href="<?php print url('node/31');?>">Get assistance with this site</a></li>
   <li class="mobile-only"><a href="#search-block-form">Jump to Search</a> </li>
   <li><a href="#navigation">Jump to menu</a></li>
   <li><a href="#content">Jump to content</a></li>
   <li><a href="#footer">Jump to footer</a></li>
 </ul>
</div>
<?php if ($page['top_section']) : ?>
  <div id="top-section">
  <?php print render($page["top_section"]); ?>
  </div>
<?php endif; ?>
<div id="page-wrapper">
  <header id="header" role="banner" class="clearfix">
    <div class="branding-holder clearfix default-width">
      <div class="crest">
        <a href="/" title="Go to the <?php print $site_name; ?> homepage" >
        <img class="clearfix" src="/<?php print path_to_theme(); ?>/images/crest.png" srcset="/<?php print path_to_theme(); ?>/images/crest.png 1x, /<?php print path_to_theme(); ?>/images/crest_2x.png 2x" alt="An image of the Australian Government crest that links to the <?php print $site_name; ?> homepage">
        </a>
      </div>

      <div id="site-name" class="clearfix">
        <div class="header-extra"><?php print render($page['header']); ?></div>
        <h1>National Strategy for International Education 2025</h1>
      </div>
    </div> <!-- /.branding-holder -->
    <?php if ($page['main_menu']) : ?>
      <nav id="main-menu" class="clearfix">
        <?php print render($page['main_menu']); ?>
      </nav>
    <?php endif;?>
  </header> <!-- /.section, /#header -->

  <div class="content-container clearfix">
    <?php if ($page['sidebar_first']):?>
    <div class="sidebar sidebar-first default-width">
      <?php
      print render($page['sidebar_first']);
      ?>
    </div> <!-- /.sidebar-second -->
    <?php endif; ?>

    <?php if($title_prefix) : ?>
      <?php print render($title_prefix); ?>
    <?php endif;?>
    <?php if ($title && !$is_front): ?>
      <div id="page-heading">
        <div class="default-width">
          <h1 class="title" id="page-title"><?php print $title; ?></h1>
        </div>
      </div>
    <?php endif; ?>
    <?php if ($title_suffix) : ?>
      <?php print render($title_suffix); ?>
    <?php endif; ?>

    <div id="content" class="clearfix" role="main">
      <?php if ($page['highlighted']): ?>
        <div id="highlighted"><?php print render($page['highlighted']); ?></div>
      <?php endif; ?>

      <?php if(!drupal_is_front_page()) : ?>
        <div class="mobile-menu default-width">
          <a href="#mobile-menu">Menu</a>
        </div>
      <?php endif; ?>

      <?php //if ($breadcrumb): ?>
       <!--  <nav id="breadcrumb"><div class="default-width"><?php //print $breadcrumb; ?></div></nav> -->
      <?php //endif; ?>
      <?php if ($tabs): ?>
        <div class="tabs default-width"><?php print render($tabs); ?></div>
      <?php endif; ?>
      <?php if(isset($page['help']) && ($page['help']))  : ?>
        <?php print render($page['help']); ?>
      <?php endif;?>

      <div id="content-wrapper">
        <div class="default-width">
          <?php print $messages; ?>
          <?php print render($page['content']); ?>
          <?php print $feed_icons; ?>
        </div>
    </div>
  </div><!-- /.content, /#content -->

  <?php if ($page['sidebar_first']):?>
    <?php if (!empty($page['sidebar_second'])):?>
      <div class="sidebar sidebar-second">
        <?php
        print render($page['sidebar_second']);
        ?>
      </div> <!-- /.sidebar-second -->
    <?php endif; ?>
  <?php endif; ?>

  <?php if ($page['download_section']): ?>
      <div id="download_section" >
          <?php print render($page['download_section']);?>
      </div>
  <?php endif; ?>

  <?php if ($page['content_bottom']): ?>
      <div id="content_bottom" class="default-width" >
          <?php print render($page['content_bottom']);?>
      </div>
  <?php endif; ?>

  <?php if ($page['testimonial']): ?>
      <div id="testimonial" >
          <?php print render($page['testimonial']);?>
      </div>
  <?php endif; ?>
</div><!-- /.content-container -->

<footer id="footer" class="wrapper full-width hover-underline">
  <div class="default-width clearfix">
    <!-- footer region -->
    <h2 class="element-invisible">Footer</h2>
    <div class="footer-region">
      <?php print render($page['footer']); ?>
    </div>
  </div>
</footer> <!-- /#footer -->
