<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php if (!$label_hidden): ?>
    <div class="field-label"<?php print $title_attributes; ?>><?php print $label ?>:&nbsp;</div>
  <?php endif; ?>
  <div class="field-items"<?php print $content_attributes; ?>>
    <?php foreach ($items as $delta => $item): ?>
      <div class="download field-item <?php print $delta % 2 ? 'odd' : 'even'; ?>">
        <?php
        // gets the attachment url //
        $uri = $item['#file']->uri;
        $external_url = file_create_url($uri);
        // gets the filename file size in kb and file type //
        $mime = $item['#file']->filemime;
        $filename = $item['#file']->filename;
        $filesize = "(" . round(($item['#file']->filesize)/1000) . "kb)";
        $file_explode = explode('.', $filename);
        $file_extension = end($file_explode);
        $doctype = nsie_theme_get_filetype_from_mime($item['#file']->filemime);
        // Fall back to using the actual filename's extension if mime didn't
        // tell us anything useful.
        ?>

        <a class="download button" type="<?php print $mime; ?>" href="<?php print $external_url ?>">
          <span class="type">Download <?php print strtoupper($doctype);?></span>
          <span class="size"><?php print strtoupper($filesize); ?></span>
        </a>
      </div>
    <?php endforeach; ?>
  </div>
</div>