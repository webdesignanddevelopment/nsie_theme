<div class="node-download">
  <div class="image"><?php print render($content['field_image']); ?></div>
  <div class="content">
    <h2><?php print $title; ?></h2>
    <?php print render($content['body']); ?>
    <?php print render($content['field_file']); ?>
  </div>
</div>