<?php
/**
 * @description: Template PHP and the following 
 				 is the functions those are used in every site in SSC
 *
 *
 */
 
/**
 * Generates a human readable file size based on bytes
 *
 * @param int $size
 *   The file size in bytes
 *
 * @return string
 */

function _file_size($size) {
  $filesizename = array("bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB");
  return $size ? round($size / pow(1024, ($i = floor(log($size, 1024))))) . ' ' . $filesizename[$i] : '0 bytes';
}

/**
 * @description: Implement hook_preprocess_html().
 */

function nsie_theme_preprocess_html(&$vars) {


}

function nsie_theme_get_filetype_from_mime($mime) {

  $mimes = array(
    'application/pdf' => 'pdf',
    'application/rtf' => 'rtf',
    'application/doc' => 'doc',
    'application/msword' => 'doc',
    'application/docx' => 'docx',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => 'docx',
    'application/xls' => 'xls',
    'application/xlsx' => 'xlsx'
  );

  if (array_key_exists($mime, $mimes)) {
    return $mimes[$mime];
  } else {
    return 'FILE';
  }
}



?>